--------------------------------------------------------------------------------
-- Helper functions
--------------------------------------------------------------------------------
function GetTableSize( t )
	if not t then
		return 0
	end
	local count = 0
	for k, v in t do
		count = count + 1
	end
	return count
end
--------------------------------------------------------------------------------
-- Logging helpers
--------------------------------------------------------------------------------
function GetStringListByArguments( argList )
	local newArgList = {}
	
	for i = 1, argList.n do
		local arg = argList[ i ]
		if common.IsWString( arg ) then
			newArgList[ i ] = arg
		else
			newArgList[ i ] = tostring( arg )
		end
	end

	return newArgList
end
--------------------------------------------------------------------------------
function LogInfo( ... )
	common.LogInfo( common.GetAddonName(), unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
function LogInfoCommon( ... )
	common.LogInfo( "common", unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
function LogWarning( ... )
	common.LogWarning( "common", unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
function LogError( ... )
	common.LogError( "common", unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
